#!/bin/sh

SOURCE_DIR="$(dirname "${BASH_SOURCE[0]}")"
export $(grep -v "^#" "${SOURCE_DIR}/.env" | xargs)

dotnet nuget add source "https://gitlab.com/api/v4/groups/${GITLAB_GROUP}/-/packages/nuget/index.json" \
  --name GitLab \
  --username $GITLAB_USERNAME \
  --password $GITLAB_ACCESS_TOKEN \
  --store-password-in-clear-text
