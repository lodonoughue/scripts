#!/bin/sh

SOURCE_DIR="$(dirname "${BASH_SOURCE[0]}")"
export $(grep -v "^#" "${SOURCE_DIR}/.env" | xargs)

docker run \
  --detach \
  --publish 1433:1433 \
  --env "ACCEPT_EULA=1" \
  --env "MSSQL_SA_PASSWORD=${AZURE_SQL_PASSWORD}" \
  --volume "${SOURCE_DIR}/../backups":"/backups" \
  --name azuresqledge \
  cagrin/azure-sql-edge-arm64
